<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

/**
 * Class aricon_options
 */
class aricon_options extends CModule
{

    /**
     * aricon_options constructor.
     */
    public function __construct()
    {

        $this->MODULE_ID = 'aricon.options';
        $this->MODULE_NAME = Loc::getMessage('ARICON_OPTIONS_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('ARICON_OPTIONS_MODULE_DESCRIPTION');

        $arModuleVersion = [];

        include __DIR__ . '/version.php';

        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
    }

    /**
     * Установка модуля
     */
    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
    }

    /**
     * Удаление модуля
     */
    public function doUninstall()
    {
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }
}

<?php

\Bitrix\Main\Loader::registerAutoloadClasses(
    "aricon.options",
    [
        "Aricon\\Options\\Model" => "lib/Model.php"
    ]
);
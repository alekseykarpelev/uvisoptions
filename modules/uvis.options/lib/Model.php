<?php
/**
 * Created by PhpStorm.
 * User: aleksey
 * Date: 15.08.19
 * Time: 14:45
 */


namespace Uvis\Options;


use COption;


/**
 * Class Model
 * @package Uvis\Options
 */
class Model
{


    /**
     *
     */
    const MODULE_ID =  'uvis.options';


    /**
     * @return mixed
     */
    public function getConfig()
    {

        $fields = include('config.php');

       return $fields;
       // echo var_dump($fields);
    }



    /**
     * @return array
     */
    public function prepareFields()

    {

        $fields = $this->getConfig();

        $out = [];

        foreach($fields as $field)
        {

         $out[] = [
                     'name'  => $field['name'],
                     'label' => $field['label'],
                     'type'  => $field['type'],
                     'value' => COption::GetOptionString(self::MODULE_ID, $field['name'])

                  ];

        }


        return $out;


    }



    /**
     *
     */
    public function updateOptions()
    {

        $checkboxes = [];

        $inputes = [];

        $textarea = [];


        foreach ($this->getConfig() as $field) {
            if ($field['type'] == 'checkbox')// находим все чекбоксы
            {
                $checkboxes[] = $field['name'];
            }

            if ($field['type'] == 'input')//  инпуты

            {
                $inputes[] = $field['name'];
            }

            if ($field['type'] == 'textarea')//  текст  поле

            {
                $textareas[] = $field['name'];
            }


        }


        if (isset($_POST['submit'])) {

            unset($_POST['submit']);


            foreach ($checkboxes as $checkbox) {

                echo $checkbox;

                if (isset($_POST[$checkbox])) {
                    COption::SetOptionString(self::MODULE_ID ,$checkbox, 'yes');

                } else {
                    COption::SetOptionString(self::MODULE_ID, $checkbox, 'no');
                }

            }

            foreach ($inputes as $inpute) {
                COption::SetOptionString(self::MODULE_ID, $inpute, $_POST[$inpute]);

            }

            foreach ($textareas as $textarea) {
                COption::SetOptionString(self::MODULE_ID, $textarea, $_POST[$textarea]);

            }

        }


        return;

    }

}





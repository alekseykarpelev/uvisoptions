<?php

\Bitrix\Main\Loader::registerAutoloadClasses(
    "uvis.options",
    [
        "Uvis\\Options\\Model" => "lib/Model.php"
    ]
);
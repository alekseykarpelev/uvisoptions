<?php

use Bitrix\Main\Localization\Loc;

use Uvis\Options\Model;




require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

IncludeModuleLangFile(__FILE__);

$moduleID = 'uvis.options';

\Bitrix\Main\Loader::includeModule($moduleID);




IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);


//$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();




if (isset($_POST['submit'])) {

    $data = new  Model;

    $data->updateOptions();

}


$model = new Model();

$fields = $model->prepareFields();

$aTabs = array(
    array("DIV"      => "id_1",   //первая закладка
        "TAB"      => "Настройки"
    ),

    array("DIV" => "id_2",        //   вторая закладка
        "TAB" => "Права доступа"

    )

);




$tabControl = new CAdminTabControl("tabControl",$aTabs);

$tabControl->Begin();

?>

<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($mid)?>&lang=<?=LANGUAGE_ID?>">

    <?php

    $tabControl->BeginNextTab();

    ?>



    <tr class="heading">
        <td colspan="2"><b>Это название блока</b></td>
    </tr>

    <tr>




<?php
foreach ($fields as $field) {

    switch ($field['type']) {
        case 'textarea':

            echo sprintf(' <tr>

        <td class="adm-detail-content-cell-l" width="">
            <label for="entity">
                %s
            </label>
        </td>

        <td class="adm-detail-content-cell-r" width="">

            <textarea   rows="10" cols="45" name="%s" >%s</textarea>

        </td>

    </tr>',$field['label'],$field['name'],$field['value']);




            break;
        case 'checkbox':

            $check = '';

            if($field['value'] == 'yes')
            {
                $check = 'checked';
            }



            echo sprintf('<tr>
        <td class="adm-detail-content-cell-l" width="">
            <label for="entity">
             %s
            </label>
        </td>

        <td class="adm-detail-content-cell-r" width="">

            <input type="checkbox" name="%s"  value="yes" %s>

        </td>



    </tr>',$field['label'],$field['name'], $check );



            break;
        default:

            echo sprintf(' <tr>

        <td class="adm-detail-content-cell-l" width="">
            <label for="entity">
                %s
            </label>
        </td>

        <td class="adm-detail-content-cell-r" width="">

            <input type="text" size="30" value="%s" name="%s">

        </td>


    </tr>',$field['label'],$field['value'],$field['name']);



            break;
    }
}

?>







    <?php

    $tabControl->buttons();

    ?>

    <input type="submit"
           name="submit"
           value="<?=Loc::getMessage("MAIN_SAVE") ?>"
           title="<?=Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
           class="adm-btn-save"
    />
    <input type="submit"
           name="restore"
           title="<?=Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
           onclick="return confirm('<?php echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
           value="<?=Loc::getMessage("MAIN_RESTORE_DEFAULTS") ?>"
    />


</form>

<?php
$tabControl->end();
?>



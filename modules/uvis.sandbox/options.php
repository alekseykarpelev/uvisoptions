<?php

use Bitrix\Main\Localization\Loc;

use Uvis\Sandbox\myClass;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

IncludeModuleLangFile(__FILE__);

$moduleID = 'uvis.sandbox';

\Bitrix\Main\Loader::includeModule($moduleID);




IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);


//$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

//UVIS_SANDBOX_MODULE_DESCRIPTION



$aTabs = array(
    array("DIV"      => "id_1",   //первая закладка
          "TAB"      => "Настройки"
         ),

    array("DIV" => "id_2",        //   вторая закладка
           "TAB" => "Права доступа"

         )

);

 // echo $mid;


$tabControl = new CAdminTabControl("tabControl",$aTabs);

$tabControl->Begin();


?>

<form method="post" action="<?php echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($mid)?>&lang=<?=LANGUAGE_ID?>&mid_menu=1">

<?php

    $tabControl->BeginNextTab();

?>


    <tr class="heading">
        <td colspan="2"><b>Это название блока</b></td>
    </tr>

    <tr>

        <td class="adm-detail-content-cell-l" width="30%">
            <label for="entity">
               Здесь какойто текст
            </label>
        </td>

        <td class="adm-detail-content-cell-r" width="70%">

            <input type="text" size="30" value="30" name="delete_after">

        </td>


    </tr>



    <tr>
        <td class="adm-detail-content-cell-l" width="30%">
            <label for="entity">
                Здесь какойто текст  #2
            </label>
        </td>

        <td class="adm-detail-content-cell-r" width="70%">

            <input type="checkbox" name="lock_catalog" id="lock_catalog" value="Y" checked>

        </td>



    </tr>

    <tr>

        <td class="adm-detail-content-cell-l" width="30%">
            <label for="entity">
               Еще какойто текст
            </label>
        </td>

        <td class="adm-detail-content-cell-r" width="70%">

          <textarea   rows="10" cols="45" name="text" > </textarea>

        </td>

    </tr>


 <?php

    $tabControl->buttons();

?>

<input type="submit"
           name="save"
           value="<?=Loc::getMessage("MAIN_SAVE") ?>"
           title="<?=Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
           class="adm-btn-save"
/>
    <input type="submit"
           name="restore"
           title="<?=Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
           onclick="return confirm('<?php echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
           value="<?=Loc::getMessage("MAIN_RESTORE_DEFAULTS") ?>"
/>


</form>

<?php
$tabControl->end();
?>















































































<?php

\Bitrix\Main\Loader::registerAutoloadClasses(
    "uvis.sandbox",
    [
        "Uvis\\Sandbox\\myClass" => "lib/myClass.php"
    ]
);